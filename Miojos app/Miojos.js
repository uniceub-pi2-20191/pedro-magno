/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Image, Text, ScrollView} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <ScrollView>
        <Image
          source={{uri: 'https://conteudo.imguol.com.br/c/entretenimento/00/2017/07/31/miojo-com-salsicha-1501515903401_v2_750x421.jpg'}}
          style={{width: 500, height: 300}}
        />
        <Text style={styles.welcome}>Salsicha e requeijão</Text>
        <Text style={styles.instructions}>Com o macarrão cozido e o tempero pronto, acrescente três salsichas picadas e uma colher de sopa de requeijão. Se quiser dar um toque a mais, grelhe os pedaços de salsicha na frigideira antes de juntar ao miojo.</Text>
         <Image
          source={{uri: 'https://conteudo.imguol.com.br/c/entretenimento/04/2017/07/31/miojo-com-ovo-e-batata-palha-1501516436227_v2_750x421.jpg'}}
          style={{width: 500, height: 300}}
        />
        <Text style={styles.welcome}>Requeijão, "ovo pochet" e batata palha</Text>
        <Text style={styles.instructions}> Durante o cozimento, misture o miojo em sentido horário até formar um redemoinho. Despeje o ovo no centro e tampe a panela. Deixe cozinhar por mais dois minutos. Tire o ovo e reserve. Retire toda a água do macarrão, acrescente duas colheres de sopa de requeijão cremoso e o tempero pronto. Para finalizar, salpique batata palha. </Text>
        <Text style={styles.instructions}></Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
